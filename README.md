# mulife
Conway's game of life from the Rust &amp; WebAssembly book.




## Credits

Pages and [GitLab CI](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_from_scratch.html)

WebAssembly in [TinyGo](https://tinygo.org/docs/guides/webassembly/)

Go 1.11 WebAssembly article by
 [Nicolas Lepage](https://medium.zenika.com/go-1-11-webassembly-for-the-gophers-ae4bb8b1ee03)

Rust &amp; WebAssembly
 [book](https://rustwasm.github.io/book/game-of-life/implementing.html)

