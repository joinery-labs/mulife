package main

import (
	"errors"
	"strings"
	"strconv"
	"syscall/js"
)

func main() {
	wait := make(chan struct{}, 0)
	js.Global().Set("formatCells", js.FuncOf(formatCells))
	<-wait
}

var        uni            *Universe
const CANVAS_ID = "drawing"

func formatCells(this js.Value, args []js.Value) interface{} {
	if sz, err := extractArg(args); err == nil {
	        if uni == nil {
	                uni = NewUniverse(sz)
	                uni.SizeGrid(CANVAS_ID)
	        }
        } else {
		// todo log error
		return nil
	}

        uni.DrawGrid(CANVAS_ID)
        uni.DrawCells(CANVAS_ID)
        uni.Tick()

	// TODO leads to the TinyGo error from wasm_exec that
	//      js.finalizeRef is not implemented
	////js.Global().Call("drawNext")
	return nil
}

func extractArg(args []js.Value) (int, error) {
        if len(args) == 0 {
		// skip config and default to 64x64
                return 64, nil
        }
        a := args[0].String()
        n, err := strconv.Atoi(a)
        if err != nil {
                return 0, errors.New("Arg required")
        }
        return n, nil
}



type Universe struct {
	width  int
	height int
	cells  []bool
}

// NewUniverse starts a new universe with arbitrary cells.
func NewUniverse(dimension int) *Universe {
	genzero := make([]bool, dimension*dimension)
	for i := 0; i < dimension*dimension; i++ {
		if i%2 == 0 || i%7 == 0 {
			genzero[i] = true
		}
	}

	return &Universe{
		width:  dimension,
		height: dimension,
		cells:  genzero,
	}
}

// Tick determines the state of the cell for the next tick of the game
func (u *Universe) Tick() {
	next := make([]bool, len(u.cells))
	copy(next, u.cells)
	for row := 0; row < u.height; row++ {
		for col := 0; col < u.width; col++ {
			i := u.cellIndex(row, col)
			alive := u.cells[i]
			aliveNext := alive
			liveCount := u.liveNeighbors(row, col)

			if alive {
				// 1. Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
				if liveCount < 2 {
					aliveNext = false
				}

				// 2. Any live cell with two or three live neighbours lives on to the next generation.
				if liveCount == 2 || liveCount == 3 {
					aliveNext = true
				}

				// 3. Any live cell with more than three live neighbours dies, as if by overpopulation.
				if liveCount > 3 {
					aliveNext = false
				}
			} else {
				// 4. Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
				if liveCount == 3 {
					aliveNext = true
				}
			}
			next[i] = aliveNext
		}
	}
	u.cells = next
}

func (u *Universe) Size() int {
	if u == nil {
		return 0
	}
	return len(u.cells)
}

const CELL_SIZE = 5
const GRID_COLOR = "#CCCCCC"
const DEAD_COLOR = "#FFFFFF"
const ALIVE_COLOR = "#000000"

func (u *Universe) SizeGrid(id string) {
	dom := js.Global().Get("document")
	can := dom.Call("getElementById", id)
	can.Set("height", (CELL_SIZE+1)*u.height+1)
	can.Set("width", (CELL_SIZE+1)*u.width+1)
}
func (u *Universe) DrawGrid(id string) {
	dom := js.Global().Get("document")
	can := dom.Call("getElementById", id)

	ctx := can.Call("getContext", "2d")

	ctx.Call("beginPath")
	ctx.Set("strokeStyle", GRID_COLOR)
	for i := 0; i <= u.width; i++ {
		ctx.Call("moveTo", i*(CELL_SIZE+1)+1, 0)
		ctx.Call("lineTo", i*(CELL_SIZE+1)+1, (CELL_SIZE+1)*u.height+1)
	}
	for j := 0; j <= u.height; j++ {
		ctx.Call("moveTo", 0, j*(CELL_SIZE+1)+1)
		ctx.Call("lineTo", (CELL_SIZE+1)*u.width+1, j*(CELL_SIZE+1)+1)
	}
	ctx.Call("stroke")
}
func (u *Universe) DrawCells(id string) {
	dom := js.Global().Get("document")
	can := dom.Call("getElementById", id)
	ctx := can.Call("getContext", "2d")

	ctx.Call("beginPath")
	for row := 0; row < u.height; row++ {
		for col := 0; col < u.width; col++ {
			i := u.cellIndex(row, col)
			if u.cells[i] {
				ctx.Set("fillStyle", ALIVE_COLOR)
			} else {
				ctx.Set("fillStyle", DEAD_COLOR)
			}
			ctx.Call(
				"fillRect",
				col*(CELL_SIZE+1)+1,
				row*(CELL_SIZE+1)+1,
				CELL_SIZE,
				CELL_SIZE,
			)
		}
	}

	ctx.Call("stroke")
}

func (u *Universe) String() string {
	var graph strings.Builder
	for k, v := range u.cells {
		if v {
			graph.WriteRune('◼')
		} else {
			graph.WriteRune('◻')
		}
		if (k+1)%u.width == 0 {
			graph.WriteString("\n")
		}
	}
	return graph.String()
}

func (u *Universe) cellIndex(row int, col int) int {
	return (row*u.width + col)
}

func (u *Universe) liveNeighbors(row int, col int) (liveCount int) {

	add := func(r, c int) {
		// If we're at an edge, check the other side of the board.
		remainderx := c % u.width
		remaindery := r % u.height

		i := u.cellIndex(remaindery, remainderx)
		if u.cells[i] {
			liveCount++
		}
	}

	// Instead of subtract (-1), add (size-1) to roll over knowing mod will be used..
	add(row+(u.height-1), col)             // up
	add(row+1, col)                        // down
	add(row, col+1)                        // To the right
	add(row, col+(u.width-1))              // To the left
	add(row+(u.height-1), col+1)           // top-right
	add(row+1, col+1)                      // bottom-right
	add(row+(u.height-1), col+(u.width-1)) // top-left
	add(row+1, col+(u.width-1))            // bottom-left
	return
}
