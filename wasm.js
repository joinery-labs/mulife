'use strict';

const WASM_URL = 'wasm.wasm';

var wasm;

function calculate() {
    window.formatCells();
}

function drawNext() {
    requestAnimationFrame(window.formatCells)
}

function init() {
    const calc = document.querySelector('#runButton');
    calc.onclick = calculate;
    calc.disabled = false;


    const go = new Go();
    if ('instantiateStreaming' in WebAssembly) {
        WebAssembly.instantiateStreaming(fetch(WASM_URL), go.importObject).then(function (obj) {
            wasm = obj.instance;
            go.run(wasm);
        })
    } else {
        fetch(WASM_URL).then(resp =>
            resp.arrayBuffer()
        ).then(bytes =>
            WebAssembly.instantiate(bytes, go.importObject).then(function (obj) {
                wasm = obj.instance;
                go.run(wasm);
            })
        )
    }
}

init();
